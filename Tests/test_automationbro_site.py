from seleniumbase import BaseCase


class HomePageTest(BaseCase):
    def test_homepage(self):
        # open automationbro home page
        self.open("https://practice.automationbro.com/")

        # Assert the title of the current web page
        self.assert_title("Practice E-Commerce Site – Automation Bro")

        # Assert that the element (logo) is visible on the page
        self.assert_element(".custom-logo-link")