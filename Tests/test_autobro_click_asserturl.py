from seleniumbase import BaseCase


class HomePageTest(BaseCase):
    def test_homepage(self):
        # open automationbro home page
        self.open("https://practice.automationbro.com/")

        # click on the get started button
        # self.click("#get-started")
        self.click("//a[@id='get-started']")

        # get the current url of the page we are directed to
        current_url = self.get_current_url()
        expected_url = "https://practice.automationbro.com/#get-started"

        # Assert the url of the current web page
        self.assert_equal(current_url, expected_url)

        # Other possible assertion for the url of the current web page
        self.assert_true("get-started" in current_url)
