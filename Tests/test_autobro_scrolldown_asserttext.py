from seleniumbase import BaseCase


class HomePageTest(BaseCase):
    def test_homepage(self):
        # open automationbro home page
        self.open("https://practice.automationbro.com/")

        # scroll down the page
        self.scroll_to_bottom()

        # assert the copyright text
        actual_copyright_text = self.get_text("//a[contains(text(),'Automation Bro')]")
        expected_copyright_text = "Copyright © 2020 Automation Bro"
        self.assert_text(expected_copyright_text, "//div[@class='tg-site-footer-section-1']")
